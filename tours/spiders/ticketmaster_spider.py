import scrapy

from tours.items import ToursItem

class QuotesSpider(scrapy.Spider):
    name = "ticketmaster"
    allowed_domains = ["http://www.ticketmaster.com/"]
    start_urls = [
            'http://www.ticketmaster.com/browse/all-music-catid-10001/music-rid-10001?calendar_from_input=01%2F23%2F2017&calendar_to_input=02%2F06%2F2017&area=&tm_link=tm_homeA_browse&rdc_select=td&type=selected'
            ]
               
    def parse(self, response):
        for href in response.xpath('//a[@class="url"]/@href'):
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_dir_contents)

    def parse_dir_contents(self, response):
        for sel in response.xpath('//div[@class="artist_info"]"]'):
            item = ToursItem()
            item['artist'] = sel.xpath('span[@id="artist_event_name"/a/text()').extract_first()
            item['city'] = sel.xpath('span[@id="artist_location"/span/text()').extract()
            item['place'] = sel.xpath('span[@id="artist_venue_name"/a/text()').extract()
            item['date'] = sel.xpath('span[@id="artist_event_date"/abbr/text()').extract()
            item['url'] = response.url
            yield item

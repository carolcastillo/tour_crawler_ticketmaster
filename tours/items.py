# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ToursItem(scrapy.Item):
    artist = scrapy.Field()
    city = scrapy.Field()
    place = scrapy.Field()
    date = scrapy.Field()
    url = scrapy.Field()